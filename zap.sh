#!/usr/bin/env bash

mkdir -p /tmp/zap_reports
chmod 777 /tmp/zap_reports

# ZAP_CONTAINER_ID=$(docker run -v /tmp/zap_reports:/zap/wrk/:rw -t owasp/zap2docker-stable zap-baseline.py -t http://172.17.0.4:8080/data_api/index.jsp -r zapReport.html)

ZAP_CONTAINER_ID=$(docker run -u zap -p 2375:2375 -d owasp/zap2docker-stable zap.sh -v /tmp/zap_reports:/zap/wrk/:rw -r zapReport.html -daemon -port 2375 -host 127.0.0.1 -config api.disablekey=true -config scanner.attackOnStart=true -config view.mode=attack -config connection.dnsTtlSuccessfulQueries=-1 -config api.addrs.addr.name=.* -config api.addrs.addr.regex=true)

echo "ZAP_CONTAINER_ID="$ZAP_CONTAINER_ID

# the target URL for ZAP to scan
TARGET_URL=$1

docker exec $ZAP_CONTAINER_ID zap-cli -p 2375 status -t 120 && docker exec $ZAP_CONTAINER_ID zap-cli -p 2375 open-url $TARGET_URL

docker exec $ZAP_CONTAINER_ID zap-cli -p 2375 spider $TARGET_URL

docker exec $ZAP_CONTAINER_ID zap-cli -p 2375 active-scan -r $TARGET_URL

docker exec $ZAP_CONTAINER_ID zap-cli -p 2375 alerts

# docker logs [container ID or name]
divider==================================================================
printf "\n"
printf "$divider"
printf "ZAP-daemon log output follows"
printf "$divider"
printf "\n"

docker logs $ZAP_CONTAINER_ID

docker stop $ZAP_CONTAINER_ID
docker rm $ZAP_CONTAINER_ID