package com.coveros.helloworld.api;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.coveros.helloworld.entity.State;


@Repository
public interface StatesRepository extends JpaRepository<State, Long> {

}
