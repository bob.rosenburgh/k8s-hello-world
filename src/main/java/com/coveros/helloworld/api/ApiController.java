package com.coveros.helloworld.api;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.coveros.helloworld.entity.State;

@RestController
@RequestMapping("/api")
public class ApiController {

	private StatesRepository repository;

	public ApiController(StatesRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/state")
	public List<State> listStates() {
		return repository.findAll(new Sort(Direction.ASC, "stateName"));
	}
}
