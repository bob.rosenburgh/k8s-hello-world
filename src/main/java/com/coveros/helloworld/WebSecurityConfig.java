package com.coveros.helloworld;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.headers().defaultsDisabled()
			.xssProtection()
			/* Let's intentionally disable these filters so that ZAP scan produces some results */
//			.and()
//			.contentTypeOptions()
//			.and()
//			.frameOptions()
			;
		
	}
}
