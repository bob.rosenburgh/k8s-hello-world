package com.coveros.helloworld.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.coveros.helloworld.api.StatesRepository;

import lombok.extern.log4j.Log4j2;


@Log4j2
@Controller
public class ApplicationController {
	
	@Autowired
	private StatesRepository repository;

	@GetMapping("/list-states")
	public String listStates(Model model) {
		log.info("listStates->");
	    model.addAttribute("states", repository.findAll(new Sort(Direction.ASC, "stateName")));
		return "states";
	}
}
