/*
 *
 * Copyright 2010 Coveros, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coveros.helloworld;

import com.coveros.selenified.Locator;
import com.coveros.selenified.Selenified;
import com.coveros.selenified.application.App;
import com.coveros.selenified.element.Element;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class UItestFT extends Selenified {

    @BeforeClass(alwaysRun = true)
    public void beforeClass(ITestContext test) {
        // set the base URL for the tests here
        setTestSite(this, test, "http://localhost/");
    }

    @Test
    public void hartfordTest(ITestContext test) {
        // use this object to manipulate the app
        App app = this.apps.get();
        // verify the correct page title
        app.goToURL(getTestSite(this.getClass().getName(), test) + "list-states");
        Element hartford = app.newElement(Locator.XPATH, "//*[contains(text(),'" + "Hartford" + "')]");
        hartford.waitForState().present();
        hartford.assertState().present();
        // verify no issues
        finish();
    }

    @Test
    public void connecticutTest(ITestContext test) {
        // use this object to manipulate the app
        App app = this.apps.get();
        // verify the correct page title
        app.goToURL(getTestSite(this.getClass().getName(), test) + "list-states");
        Element connecticut = app.newElement(Locator.XPATH, "//*[contains(text(),'" + "Connecticut" + "')]");
        connecticut.waitForState().present();
        connecticut.assertState().present();
        // verify no issues
        finish();
    }
}
