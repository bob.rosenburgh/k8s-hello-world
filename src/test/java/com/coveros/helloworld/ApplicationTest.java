package com.coveros.helloworld;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ApplicationTest {

	@Autowired
	private ApplicationContext context;
	
	@Test
	public void contextLoads() {
		assertNotNull(context);
	}

}
