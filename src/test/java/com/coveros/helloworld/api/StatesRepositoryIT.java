package com.coveros.helloworld.api;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import com.coveros.helloworld.entity.State;

@RunWith(SpringRunner.class)
@DataJpaTest
public class StatesRepositoryIT {

	@Autowired
	private StatesRepository repository;
	
	@Test
	public void testFindAllSort() {
		List<State> states = repository.findAll(new Sort(Direction.ASC, "stateName"));
		assertEquals("Alabama", states.get(0).getStateName());
		assertEquals("Wyoming", states.get(states.size()-1).getStateName());
	}

}
