
# Start with a base image containing Java runtime
FROM openjdk:8-jdk-alpine

# The application's jar file
ARG JAR_FILE=target/k8s-hello-world-1.0.0.jar

# Add the application's jar to the container
ADD ${JAR_FILE} k8s-hello-world.jar

ENV DB_URL=$DB_URL

# Run the jar file 
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/k8s-hello-world.jar"]