  def label = "worker-${UUID.randomUUID().toString()}"

  // Hello world App settings
  def APP_HOST = "52.203.157.75"
  def APP_NODE_PORT = 32500
  def APP_URL = null
  def MYSQL_HOST = "helloworld-mysql"
  def RELEASE_NAME = "helloworld"
  def CHART_PATH = "./helloworld-chart"

  //SonarQube
  def SONAR_URL = "http://jenkins-demo-sonarqube:9000"

  // ZAP
  def ZAP_HOST = "jenkins-demo-zap"
  def ZAP_PORT = 8080

  // Nexus
  def NEXUS_REPO_EXTERNAL_HOST = "localhost"
  def NEXUS_REPO_PROXY_PORT = 32002
  def NEXUS_REPO_HOST = null
  def NEXUS_REPO_PORT = null

  podTemplate(label: label, containers: [
      containerTemplate(name: 'selenium', image: 'markhobson/maven-chrome:latest', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'maven', image: 'maven:3.3.9-jdk-8-alpine', ttyEnabled: true, command: 'cat'),
      containerTemplate(name: 'kubectl', image: 'lachlanevenson/k8s-kubectl:latest', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'helm', image: 'lachlanevenson/k8s-helm:latest', command: 'cat', ttyEnabled: true),
      /*
          Used for doing a PUT to Nexus Helm repo of latest application helm chart. wget insufficient and curl not
          present in busybox image.

          TODO: better approach rather than spinning up a new container?
      */
      containerTemplate(name: 'busybox', image: 'yauritux/busybox-curl', command: 'cat', ttyEnabled: true)
  ],
  volumes: [
    hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock'),
    /*
          To support maven dependency cache.  TODO: this can be an issue when running concurrent builds.
          The real answer is to create a clonable PV that writes a maven cache operator for k8s.
    */
    persistentVolumeClaim(mountPath: '/root/.m2/repository', claimName: 'jenkins-demo', readOnly: false)
  ]) {
      node(label) {

          sh "env"
          APP_URL = "http://${APP_HOST}:${APP_NODE_PORT}/"
          echo "APP_URL=${APP_URL}"
          NEXUS_REPO_HOST = "${env.SONATYPE_NEXUS_SERVICE_SERVICE_HOST}"
          NEXUS_REPO_PORT = env.SONATYPE_NEXUS_SERVICE_SERVICE_PORT_HTTP

          try {
              def appImage;
              stage('Checkout') {
                  echo "checking out app"
                  git branch: 'master', url: 'https://gitlab.com/bob.rosenburgh/k8s-hello-world.git'
                  // sh "ls -a"
              }

              container("docker") {
                  docker.withRegistry("http://${NEXUS_REPO_EXTERNAL_HOST}:${NEXUS_REPO_PROXY_PORT}",
                  'nexus') {
                      stage('Build') {
                          container('maven') {
                              sh 'mvn clean package -Dskip.unit.tests'
                          }
                          echo "Building the demo app"
                          appImage = docker.build("helloworld:latest")
                      }
                      /*
                       Parallel block that runs unit and integration tests at the same time. This is an overall time savings allowing
                       integration tests
                      */
                      stage("Continuous Integration Tests") {

                          parallel UnitTests: {

                              stage("Unit Tests") {
                                  /*
                                  Run unit tests in the maven container against docker image.
                                  */
                                  container('maven') {
                                      try {
                                          sh "mvn test -Dmaven.surefire.includes='**/*Test.java'"
                                      } catch (e) {
                                          throw e
                                      } finally {
                                          junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'
                                          sh "rm -rf ci-unit"
                                          sh "cp -r target/surefire-reports ci-unit"
                                          publishHTML(target: [
                                                  allowMissing         : false,
                                                  alwaysLinkToLastBuild: true,
                                                  keepAll              : true,
                                                  reportDir            : 'ci-unit',
                                                  reportFiles          : 'index.html',
                                                  reportName           : "Unit Tests"])
                                      }
                                  }
                              }
                          },
                          IntegrationTest: {

                              stage("Integration Tests") {
                                  container('maven') {
                                      echo 'Run integration tests'
                                      try {
                                           sh "mvn test -Ddependency-check.skip=true -Dmaven.surefire.includes='**/*IT.java' -Dalt.build.dir=int"
                                      } finally {
                                           junit allowEmptyResults: true, testResults: 'int/surefire-reports/*.xml'
                                           sh "rm -rf ci-integration"
                                           sh "cp -r int/surefire-reports ci-integration"
                                           publishHTML(target: [
                                                   allowMissing         : false,
                                                   alwaysLinkToLastBuild: true,
                                                   keepAll              : true,
                                                   reportDir            : 'ci-integration',
                                                   reportFiles          : 'index.html',
                                                   reportName           : "Integration Tests"])
                                      }
                                  }
                              }
                          }
                      }

                      stage("Static Code Analysis") {
                          parallel QualityScan: {
                              stage("SonarQube Quality Analysis") {
                                  jacoco()
                                  /*
                                  The Int and unit tests produce coverage reslults. We run the static analysis at the end so that
                                  the coverage results can be uploaded at the same time the static analysis reslults are.
                                  */
                                  container('maven') {
                                      sh "mvn sonar:sonar -Dsonar.host.url=${SONAR_URL}"
                                  }
                              }
                          },
                          SecurityScan: {
                              stage("Static Security Analysis") {
                                  /*
                                  Using tools such as Find Security Bugs or Fortify, scan your source code
                                  for potential insecure code.
                                  */
                                  try {
                                      // Run the security analysis goal
                                      //sh "mvn sonar:sonar"
                                      container('maven') {
                                        sh("./working.sh")
                                      }
                                  } catch (err) {
                                      throw err
                                  }
                              }
                          },
                          DependencyScan: {
                              stage("3rd Party Library Analysis") {
                                  /*
                                  Use a tool such as OWASP Dependency Check or Nexus IQ to scan your application for
                                  vulnerabilities in 3rd party libraries
                                  */
                                  container('maven') {
                                      try {
                                          sh "mvn dependency-check:check"
                                      } catch (err) {
                                          throw err
                                      } finally {
                                          sh "mv ./target/dependency-check-report.* ."
                                          archiveArtifacts artifacts: 'dependency-check-report.html'
                                          dependencyCheckPublisher canComputeNew: false, defaultEncoding: '', healthy: '', pattern: 'dependency-check-report.xml', unHealthy: ''
                                      }
                                  }
                              }
                          }
                      }

                      stage("Deploy to QA") {
                          /* Push docker image to repo */
                          stage("Publish Docker Image") {
                            appImage.push()
                          }
                          withCredentials([
                                 usernamePassword(
                                         credentialsId: 'nexus',
                                         usernameVariable: 'nexususername',
                                         passwordVariable: 'nexuspassword'
                                 )
                          ]) {
                              stage("Publish Helm Chart") {
                                  /* Publish helm chart to repo */
                                  container('helm') {
                                      sh "helm init --client-only"
                                      sh "helm package --save=false ${CHART_PATH}"
                                  }
                                  container('busybox') {
                                      /* Push chart to Nexus helm repo */
                                      sh "curl -u ${nexususername}:${nexuspassword} -v http://${NEXUS_REPO_HOST}:${NEXUS_REPO_PORT}/repository/helm/ --upload-file helloworld-chart-0.1.0.tgz"
                                  }

                              }

                              stage("Install Helm Chart") {
                                  container('helm') {
                                      sh "helm upgrade --install --timeout 300 --wait ${RELEASE_NAME} --set mysql.mysqlHost=${MYSQL_HOST},imageCredentials.registry=${NEXUS_REPO_EXTERNAL_HOST}:${NEXUS_REPO_PROXY_PORT}/helloworld,imageCredentials.username=${nexususername},imageCredentials.password=${nexuspassword},image.repository=${NEXUS_REPO_EXTERNAL_HOST}:${NEXUS_REPO_PROXY_PORT}/helloworld,image.tag=latest ${CHART_PATH}"
                                  }
                              }
                          }
                      }

                      stage("Post-Deployment Checks") {
                          parallel SmokeTest: {
                              try {
                                 echo 'Perform smoke test for system viability'
                                 sh "wget ${APP_URL}list-states &>/dev/null"
                              } catch (err) {
                                 echo "Error found running the smoke test."
                                 currentBuild.result = "FAILURE"
                                 throw err
                              }
                          },
                          IntegrationTest: {
                              container('maven') {
                                  echo 'Run integration tests in Test environment'
                                  try {
                                     sh "mvn test -Ddependency-check.skip=true -DappURL=${APP_URL}"
                                  } finally {
                                      junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'
                                      sh "rm -rf qa-integration"
                                      sh "mv target/surefire-reports qa-integration"
                                      publishHTML(target: [
                                             allowMissing         : false,
                                             alwaysLinkToLastBuild: true,
                                             keepAll              : true,
                                             reportDir            : 'qa-integration',
                                             reportFiles          : 'index.html',
                                             reportName           : "QA Smoke Tests"])
                                  }
                              }
                          }
                      }
                  }
              }
              stage("Functional Tests") {

                  parallel TestLocal: {
                      container('selenium') {
                          try {
                                sh "mvn verify -Dskip.unit.tests=true -Ddependency-check.skip=true -DappURL=${APP_URL} -Dproxy=${ZAP_HOST}:${ZAP_PORT} -Dbrowser='chrome' -Dheadless"
                          } catch(err) {
                            throw err
                          } finally {
                              sh 'mkdir -p zap-proxy'
                              sh "wget -q -O zap-proxy/report.html http://${ZAP_HOST}:${ZAP_PORT}/OTHER/core/other/htmlreport"
                              sh "wget -q -O zap-proxy/report.xml http://${ZAP_HOST}:${ZAP_PORT}/OTHER/core/other/xmlreport"
                              publishHTML([
                                      allowMissing         : false,
                                      alwaysLinkToLastBuild: true,
                                      keepAll              : true,
                                      reportDir            : 'zap-proxy',
                                      reportFiles          : 'report.html',
                                      reportName           : 'ZAP Proxy Report'
                              ])
                              junit allowEmptyResults: true, testResults: 'target/failsafe-reports/*.xml'
                              sh "rm -rf qa-local"
                              sh "mv target/failsafe-reports qa-local"
                              publishHTML(target: [
                                      allowMissing         : false,
                                      alwaysLinkToLastBuild: true,
                                      keepAll              : true,
                                      reportDir            : 'qa-local',
                                      reportFiles          : 'index.html',
                                      reportName           : "Local Functional Browser Tests"])
                          }
                      }
                  },
                  TestRemote: {
                    echo 'Run test suite on sauce labs'
                    container('maven') {
                      sh("./working.sh")
                    }
                  }
              }

              stage("Dynamic Code Analysis") {
                  parallel QualityScan: {
                      stage("SonarQube Quality Analysis") {
                          /*
                          The Int and unit tests produce coverage reslults. We run the static analysis at the end so that
                          the coverage results can be uploaded at the same time the static analysis reslults are.
                          */
                          container("maven") {
                              //Gather the it tests Gather the int coverage results
                              sh "mvn sonar:sonar -Dsonar.host.url=${SONAR_URL}"
                          }
                      }
                  }
              }

              stage("Deploy to Staging") {
                  echo 'Deploying container to staging environment for functional testing'
                  container('maven') {
                      sh("./working.sh")
                  }
              }

              stage("Post-Deployment Checks") {
                  parallel SmokeTest: {
                      try {
                          echo 'Perform smoke test for system viability'
                          sh "wget -qO- ${APP_URL}api/state &>/dev/null"
                      } catch (err) {
                          echo "Error found running the smoke test."
                          currentBuild.result = "FAILURE"
                          throw err
                      }
                  },
                  IntegrationTest: {
                      echo 'Run integration tests in Stage environment'
                      container('maven') {
                          try {
                              sh "mvn test -Ddependency-check.skip=true -DappURL=${APP_URL}"
                          } finally {
                              junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'
                              sh "rm -rf staging-integration"
                              sh "mv target/surefire-reports staging-integration"
                              publishHTML(target: [
                                      allowMissing         : false,
                                      alwaysLinkToLastBuild: true,
                                      keepAll              : true,
                                      reportDir            : 'staging-integration',
                                      reportFiles          : 'index.html',
                                      reportName           : "Staging Smoke Tests"])
                          }
                      }
                  }
              }

              stage("Non-Functional Tests") {
                  parallel PerformanceTest: {
                      echo 'Run performance test suite'
                      container('maven') {
                          sh("./working.sh")
                      }
                  },
                  LoadTest: {
                      echo 'Run load testing scenarios'
                      container('maven') {
                          sh("./working.sh")
                      }
                  },
                  Acessibility: {
                      echo 'Run Section 508 Compliance Scan'
                      container('maven') {
                          sh("./working.sh")
                      }
                  },
                  DynamicSecurityTest: {
                      container('maven') {
                          sh("./working.sh")
                      }
                  }
              }
              stage("Deploy to Prod") {
                  echo 'Deploying application to production'
                  container('maven') {
                      sh("./working.sh")
                  }
              }
              stage("Post-Deployment Checks") {
                  parallel SmokeTest: {
                      echo 'Perform smoke test for system viability'
                      container('maven') {
                          sh("./working.sh")
                      }
                  } //,
                  // Let's not do integration tests in Prod
                  // IntegrationTest: {
                  //     container('maven') {
                  //         echo 'Run integration tests in Prod environment'
                  //         try {
                  //           sh "mvn test -Ddependency-check.skip=true -DappURL=${APP_URL}"
                  //         } finally {
                  //             junit allowEmptyResults: true, testResults: 'target/surefire-reports/*.xml'
                  //             sh "rm -rf prod-integration"
                  //             sh "mv target/surefire-reports prod-integration"
                  //             publishHTML(target: [
                  //                     allowMissing         : false,
                  //                     alwaysLinkToLastBuild: true,
                  //                     keepAll              : true,
                  //                     reportDir            : 'prod-integration',
                  //                     reportFiles          : 'index.html',
                  //                     reportName           : "Production Smoke Tests"])
                  //         }
                  //     }
                  // }
              }

      } catch(err) {
          container('helm') {
              echo "Deleting the release, ${RELEASE_NAME}, from Kubernetes cluster"
              /*  --purge removes the release from the store and make its name free for later use */
              try {
                  sh "helm delete --purge ${RELEASE_NAME}"
              } catch(err1) {
                echo "Problem deleting application: ${err1}"
              }
          }
          stage("Error") {
              currentBuild.result = "FAILURE"
              throw err
          }
      } finally {

            step([$class: 'LogParserPublisher', projectRulePath: "${pwd()}/log-parser-rules", useProjectRule: true, showGraphs: true])

            stage("Pipeline Clean-Up") {
    //          parallel ArchiveTestResults: {
    //              stage ("Archive UI Test Screenshots") {
    //                archiveArtifacts artifacts: 'screenshots/*.png'
    //              }
    //          },
                parallel StopContainers: {
                    stage("Stopping Containers") {
                        if (currentBuild.result == "FAILURE") {
                            stage("State Capture") {
                              echo "TODO: state capture"
                            }
                        }
                    }
                },
                CleanWorkspace: {
                    stage("Clean Workspace") {
                        echo 'Cleaning up workspace following a successful build and deploy'
                    }
                }
            }
        }
      }
  }
